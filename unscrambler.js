function anagrams(word, anas) {
    //if there's no word, there's no anagram
    if (!word || !word.length) return null

    return anas[ //look our word up in our pre-existing list of anagrams (feels like cheating doesn't it?)
        word.toLowerCase()
            .split('')
            .sort() // make sure the letters are in alphabetical order, so if we were looking up anagrams for 'bat', we'd use 'abt'
            .join('')
        ];
}

/*
 * Unscrambles anagrams in a paragraph
 */
function unscramble(inputText, freqs, anas) {
    return inputText
        .toLocaleLowerCase() //make everything lowercase because I'm Laaaaazy
        .split('(').join(' ( ') //remove most punctuation and newlines
        .split(')').join(' ) ')
        .split(',').join('')
        .split("’").join('')
        .split("'").join('')
        .split("!").join('')
        .split("“").join('')
        .split("”").join('')
        .split('"').join('')
        .split("?").join('')
        .split(";").join('')
        .split('\n').join(' ')
        .split(' ') //split up the whole thing into individual words
        .map(word => { //take each word and
            if (!word) { return ''; } //if it's empty (like if we had two spaces in a row or a word that was only punctuation) ignore it
            
            
            //check to see if there's a period on this word and if there is, remember it!
            let punctuation = '';
            if (word.indexOf('.') > -1) {
                punctuation = '.';
                word = word.split('.').join('');
            }

            //get anagrams for this word!
            const words = anagrams(word, anas);
            
            //if there are no anagrams for this word, leave it as-is
            if (words === undefined || words === null) {
                return `{${word}}` + punctuation;
            }

            //if there's just one anagram, our answer is obviously that
            if (words.length === 1) { return words[0] + punctuation; }

            //if there's more than one anagram, check our list of word frequencies to see which is the most common
            for (const w of freqs) {
                if (words.indexOf(w) > -1) {
                    return w + punctuation;
                }
            }

            //code really shouldn't ever get here but just in case I didn't think of something, just spit out the first anagram anyway
            return words[0] + punctuation;
        })
        .join(' '); //smash all the words together again with spaces between them and that's our answer!
}

(async function () {
    //huge list of words, ordered by how common they are (more common words should be picked first!)
    const freqs = await (
        await fetch(
            browser.runtime.getURL("freqs.json")
        )
    ).json();

    //open up two anagram lists
    const anas1 = await (
        await fetch(
            browser.runtime.getURL("anagrams1.json")
        )
    ).json();

    const anas2 = await (
        await fetch(browser.runtime.getURL("anagrams2.json")
        )
    ).json();

    const anas = { ...anas1, ...anas2 }; //combine the two lists

    //select all <p> elements within <div> tags in the page's HTML (this is just how the site is formatted)
    const elements = document.querySelectorAll('div p');

    //for each one
    for (const e of elements) {
        //unscramble it using our list of anagrams and most frequent words
        e.innerHTML = unscramble(e.innerHTML, freqs, anas);
    }
})();
